##### Запуск проекта
docker-compose up -d

##### Посмотреть api можно по адресу
http://test.localhost/api

##### Пример создания товара    
    POST /api/products HTTP/1.1
    Host: test.localhost
    Content-Type: application/json
    
    {
      "title": "product1",
      "price": "100.50"
    }

##### Пример создания заказа    
    POST /api/orders HTTP/1.1
    Host: test.localhost
    Accept: application/json
    Content-Type: application/json
    
    {
        "products": [
            {
                "count": 2,
                "product": "/api/products/d86274e1-407e-409b-90d9-2251cd6cbd37"
            }
        ]
    }

##### Пример редактирования статуса

    PUT /api/orders/eb84099f-2253-4d1a-a3a2-131ba8ba543d/change-status HTTP/1.1
    Host: test.localhost
    Accept: application/json
    Content-Type: application/json
    
    {
        "status": "processed"
    }


##### Получение списка заказов    
    GET /api/orders HTTP/1.1
    Host: test.localhost
    Accept: application/json
    
##### Создание заказа через консоль
    bin/console app:create-order id:количество id2:количество
    
    пример
    bin/console app:create-order d86274e1-407e-409b-90d9-2251cd6cbd37:2 06064b8b-cef4-4fc8-ae2d-4
    6a3be815e65:4    