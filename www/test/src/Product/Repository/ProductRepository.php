<?php

declare(strict_types=1);


namespace App\Product\Repository;


use App\Product\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?Product
    {
        return parent::find(...func_get_args());
    }

    public function findOneBy(array $criteria, array $orderBy = null): ?Product
    {
        return parent::findOneBy(...func_get_args());
    }
}