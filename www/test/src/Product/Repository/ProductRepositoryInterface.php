<?php

declare(strict_types=1);


namespace App\Product\Repository;


use App\Product\Entity\Product;

interface ProductRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null): ?Product;

    public function findOneBy(array $criteria, array $orderBy = null): ?Product;
}