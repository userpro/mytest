<?php

declare(strict_types=1);


namespace App\Product\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\GeneratedValue;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 * @ApiResource(
 *     iri="http://schema.org/Product",
 *     collectionOperations={"get", "post"},
 *     itemOperations={"get"}
 * )
 */
class Product
{
    /**
     * Точность(количество знаков после запятой) цены
     */
    public const PRICE_SCALE = 2;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups("orderRead")
     */
    private LazyUuidFromString $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * @var string
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private string $price;

    public function __construct(string $title, string $price)
    {
        $this->title = $title;
        $this->price = $price;
    }

    /**
     * @return LazyUuidFromString
     */
    public function getId(): LazyUuidFromString
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }



}