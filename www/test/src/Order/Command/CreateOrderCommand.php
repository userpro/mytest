<?php

declare(strict_types=1);


namespace App\Order\Command;


use App\Order\Entity\Order;
use App\Order\Entity\OrderProduct;
use App\Product\Repository\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CreateOrderCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-order';
    /**
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ProductRepositoryInterface $productRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Создание заказа')
            ->setHelp('')
            ->addArgument(
                'products',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'Введите товары в виде ID_ТОВАРА:КОЛИЧЕСТВО'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputProducts = $input->getArgument('products');
        foreach ($inputProducts as $inputProduct) {
            list($id, $count) = explode(':', $inputProduct);

            $product = $this->productRepository->find($id);
            if (!$product) {
                throw new \Exception("Товар с id {$id} не найден");
            }
            $orderProduct = new OrderProduct((int) $count, $product);
            $orderProductArray[] = $orderProduct;
        }

        $order = new Order($orderProductArray);
        $this->entityManager->persist($order);
        $this->entityManager->flush();

        $output->writeln("Заказ номер {$order->getId()} успешно создан, сумма заказа {$order->getSum()} руб.");

        return Command::SUCCESS;
    }
}