<?php

declare(strict_types=1);


namespace App\Order\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Order\Dto\CreateOrder\CreateOrder;
use App\Order\Entity\Order;
use App\Order\Service\OrderServiceInterface;

class OrderDataTransformer implements DataTransformerInterface
{
    /**
     * @var OrderServiceInterface
     */
    private OrderServiceInterface $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    public function transform($object, string $to, array $context = []): Order
    {
        return $this->orderService->create($object);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof CreateOrder) {
            return false;
        }

        return Order::class === $to && null !== ($context['input']['class'] ?? null) && 'create' === ($context['collection_operation_name'] ?? null);
    }
}