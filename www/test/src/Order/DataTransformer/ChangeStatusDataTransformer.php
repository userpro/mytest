<?php

declare(strict_types=1);


namespace App\Order\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Order\Dto\CreateOrder\ChangeStatus\ChangeStatus;
use App\Order\Entity\Order;
use App\Order\Service\OrderServiceInterface;

class ChangeStatusDataTransformer implements DataTransformerInterface
{
    /**
     * @var OrderServiceInterface
     */
    private OrderServiceInterface $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    public function transform($object, string $to, array $context = []): Order
    {
        $order = $context['object_to_populate'];

        return $this->orderService->changeStatus($order, $object->status);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ChangeStatus) {
            return false;
        }

        return Order::class === $to && null !== ($context['input']['class'] ?? null) && 'changeStatus' === ($context['item_operation_name'] ?? null);
    }
}