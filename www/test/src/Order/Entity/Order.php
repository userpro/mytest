<?php

declare(strict_types=1);


namespace App\Order\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Product\Entity\Product;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\GeneratedValue;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity
 * @ORM\Table(name="orders")
 * @ApiResource(
 *     normalizationContext={"groups": {"orderRead"}},
 *     collectionOperations={
 *         "create"={
 *                 "method"="POST",
 *                 "input"=App\Order\Dto\CreateOrder\CreateOrder::class
 *             },
 *         "get"
 *         },
 *     itemOperations={
 *         "get",
 *         "changeStatus"={
 *             "method"="PUT",
 *             "path"="orders/{id}/change-status",
 *             "input"=App\Order\Dto\ChangeStatus\ChangeStatus::class
 *         }
 *     }
 * )
 */
class Order
{
    // Создан
    public const STATUS_CREATED = 'created';

    // Обработан
    public const STATUS_PROCESSED = 'processed';

    // Передан курьеру
    public const STATUS_TRANSFERRED = 'transferred';

    // Выполнен
    public const STATUS_COMPLETED = 'completed';

    // Отменен
    public const STATUS_CANCELED = 'canceled';

    public const STATUSES = [
        0 => self::STATUS_CREATED,
        1 => self::STATUS_PROCESSED,
        2 => self::STATUS_TRANSFERRED,
        3 => self::STATUS_COMPLETED,
        4 => self::STATUS_CANCELED,
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @Groups("orderRead")
     */
    private LazyUuidFromString $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("orderRead")
     */
    private \DateTimeInterface $created;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups("orderRead")
     */
    private int $status = 0;

    /**
     * @var string
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups("orderRead")
     */
    private string $sum = "0";

    /**
     * @var OrderProduct[]
     * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="order", cascade={"persist"})
     * @Groups("orderRead")
     */
    private $products;

    public function __construct($products)
    {
        $this->created = new DateTimeImmutable();
        $this->products = $products;
        foreach($this->products as $product) {
            $product->setOrder($this);
            $momentSum = \bcmul($product->getMomentPrice(), (string) $product->getCount());
            $this->sum = \bcadd($this->sum, $momentSum, Product::PRICE_SCALE);
        }
    }

    /**
     * @return LazyUuidFromString
     */
    public function getId(): LazyUuidFromString
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return self::STATUSES[$this->status];
    }

    /**
     * @return string
     */
    public function getSum(): string
    {
        return $this->sum;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $reverseStatuses = array_flip(self::STATUSES);

        if (!isset($reverseStatuses[$status])) {
            throw new \Exception("Invalid status $status");
        }

        $this->status = $reverseStatuses[$status];
    }

    /**
     * @return OrderProduct[]
     */
    public function getProducts()
    {
        return $this->products;
    }
}