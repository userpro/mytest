<?php

declare(strict_types=1);


namespace App\Order\Entity;


use App\Product\Entity\Product;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\GeneratedValue;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity
 * @ORM\Table(name="order_products")
 */
class OrderProduct
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private LazyUuidFromString $id;

    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="products")
     */
    private Order $order;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups("orderRead")
     */
    private int $count;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="App\Product\Entity\Product")
     * @Groups("orderRead")
     */
    private Product $product;

    /**
     * @var string
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups("orderRead")
     */
    private string $momentPrice;

    /**
     * @param int $count
     * @param Product $product
     */
    public function __construct(int $count, Product $product)
    {
        $this->count = $count;
        $this->product = $product;
        $this->momentPrice = $product->getPrice();
    }

    /**
     * @return LazyUuidFromString
     */
    public function getId(): LazyUuidFromString
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getMomentPrice(): string
    {
        return $this->momentPrice;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }



}