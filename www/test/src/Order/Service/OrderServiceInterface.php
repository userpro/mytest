<?php

declare(strict_types=1);


namespace App\Order\Service;


use App\Order\Dto\CreateOrder\CreateOrder;
use App\Order\Entity\Order;

interface OrderServiceInterface
{
    public function create(CreateOrder $dto): Order;

    public function changeStatus(Order $order, string $newStatus): Order;
}