<?php

declare(strict_types=1);


namespace App\Order\Service;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Order\Dto\CreateOrder\CreateOrder;
use App\Order\Entity\Order;
use App\Order\Entity\OrderProduct;
use Symfony\Component\Workflow\WorkflowInterface;

class OrderService implements OrderServiceInterface
{
    /**
     * @var WorkflowInterface
     */
    private WorkflowInterface $orderStateMachine;
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    public function __construct(WorkflowInterface $orderStateMachine, ValidatorInterface $validator)
    {
        $this->orderStateMachine = $orderStateMachine;
        $this->validator = $validator;
    }

    public function create(CreateOrder $dto): Order
    {
        $this->validator->validate($dto);

        $productsInfo = array_map(function($productInfo) {
            return new OrderProduct($productInfo->count, $productInfo->product);
        }, $dto->products);

        $order = new Order($productsInfo);

        return $order;
    }

    public function changeStatus(Order $order, string $newStatus): Order
    {
        $this->orderStateMachine->apply($order, $newStatus);

        return $order;
    }
}