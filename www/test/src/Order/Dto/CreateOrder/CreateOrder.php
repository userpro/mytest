<?php

declare(strict_types=1);


namespace App\Order\Dto\CreateOrder;


use Symfony\Component\Validator\Constraints as Assert;

class CreateOrder
{
    /**
     * @var ProductInfo[]
     * @Assert\Valid()
     */
    public array $products;
}