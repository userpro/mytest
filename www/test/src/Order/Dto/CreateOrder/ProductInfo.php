<?php

declare(strict_types=1);


namespace App\Order\Dto\CreateOrder;


use App\Product\Entity\Product;
use Symfony\Component\Validator\Constraints as Assert;

class ProductInfo
{
    /**
     * @var int
     * @Assert\Positive()
     */
    public int $count;

    public Product $product;
}