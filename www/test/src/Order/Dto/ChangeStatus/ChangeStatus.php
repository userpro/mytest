<?php

declare(strict_types=1);


namespace App\Order\Dto\ChangeStatus;


class ChangeStatus
{
    public string $status;
}